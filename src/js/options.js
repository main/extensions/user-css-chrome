/* global CodeMirror */

jQuery(function() {
	const editor = $("#user_css");

	editor.attr('placeholder',
		"## GLOBAL\n" +
		"{ global rules go here }\n" +
		"## example.com,sample.com\n"+
		"{ rules for example.com or sample.com }\n");

	const code_editor = CodeMirror.fromTextArea(editor[0],
		{
			mode: 'css',
			lineNumbers: 1,
			indentUnit: 5,
			lineWrapping: 0,
			matchBrackets: 1,
			autoCloseBrackets: 1
		});

	chrome.storage.sync.get('user_css', function(val) {
		code_editor.setValue(val.user_css || "");
	});

	$("#options").on('submit', function() {
		const user_css = code_editor.getValue();

		chrome.storage.sync.set({user_css: user_css}, () => {
			$("#message").html("Data saved");
		});

		return false;
	});

});
