chrome.runtime.sendMessage({method: "get-css", href: location.href}, function(response) {
	console.log('got styles', response);

	$("head").append($("<style>")
		.attr('type', 'text/css')
		.text(response.user_css));
});

