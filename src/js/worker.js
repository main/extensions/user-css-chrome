/*chrome.storage.sync.get('user_css', (ret) => {
	if (typeof ret.user_css != 'undefined') {
		localStorage['user_css'] = ret.user_css;
	}
});

chrome.storage.onChanged.addListener((changes, namespace) => {
   if (namespace == "sync" && typeof changes.user_css != "undefined") {
		console.log("settings changed", changes);
		localStorage['user_css'] = changes.user_css.newValue;
   }
});*/

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.method == "get-css") {

		console.log('requested styles for', request.href);

		chrome.storage.sync.get('user_css', (ret) => {
			const css_data = ret.user_css;
			const tmp = [];

			if (css_data) {
				const lines = css_data.split("\n");

				let section = "";

				for (let i = 0; i < lines.length; i++) {
					const line = lines[i];

					if (line.indexOf("##") == 0) {
						section = line.substr(2).trim().split(",");
						continue;
					}

					if (section[0] == "GLOBAL" || section.length == 0) {
						tmp.push(line);
					} else {
						for (let j = 0; j < section.length; j++) {
							if (request.href.indexOf(section[j].trim()) != -1) {
								tmp.push(line);
							}
						}
					}
				}
			}

			sendResponse({user_css: tmp.join("\n")});
		});

		// return true because our response is asynchronous
		return true;

	} else {
		console.warn('unknown method', request.method);
		sendResponse({});
	}
});
